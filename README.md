METODOLOGÍAS Y MODELADO DE DESARROLLO DE SOFTWARE

UNIDADES DE APRENDIZAJE 

########################################################

1.	Unidad de aprendizaje	I. Ingeniería de requerimientos
2.	Horas Teóricas	3
3.	Horas Prácticas	6
4.	Horas Totales	9
5.	Objetivo de la Unidad de Aprendizaje	El alumno realizará el análisis de problemas mediante técnicas de recolección de información  para generar el documento de especificación de requerimientos de un proyecto de software.



Temas
Técnicas de recolección de requerimientos: Entrevistas, encuestas, observación y listas de verificación.
Análisis y síntesis de información.
Especificación y validación de requerimientos. IEEE-830 y plantillas SRS.

Saber
Identificar las técnicas de recolección de requerimientos para un proyecto de desarrollo de software (Entrevistas, encuestas, observación y listas de verificación).
Definir los tipos de requerimientos para un proyecto de software de acuerdo al dominio de la aplicación.
Distinguir los requerimientos de software de acuerdo al estándar IEEE 830-1998, utilizando técnicas de validación de requerimientos.

Saber Hacer
Diseñar las herramientas  para la recolección de datos como: guía de entrevista, encuesta, guía de observación y  lista de verificación.
Clasificar los requerimientos para un proyecto de software.
Proponer la plantilla adecuada para el tipo de proyecto de acuerdo al estándar IEEE830.

Ser
Sistemático.
Analítico.
Observador.
Crítico.
Colaborativo.
Ético.

########################################################

1.	Unidad de aprendizaje	II. Modelado de software
2.	Horas Teóricas	11
3.	Horas Prácticas	18
4.	Horas Totales	29
5.	Objetivo de la Unidad de Aprendizaje	El alumno construirá los modelos de proyecto de software con base a un tipo de arquitectura definida para dar solución a casos establecidos.

Temas
Tipos de arquitecturas: SOA, Micro servicios, cliente - servidor, monolítica, distribuido, capas.
Modelado UML.

Saber
Definir los tipos de arquitectura: SOA, Micro servicios, cliente - servidor, monolítica, distribuido, capas.
Identificar la estructura del lenguaje UML de acuerdo a las áreas estructural, dinámica, gestión del modelo y extensiones (Casos de uso, diagrama de clases, Diagramas de actividades, secuencia, componentes, despliegue).

Saber Hacer
Esquematizar la perspectiva del proyecto empleando vistas de la arquitectura.
Diseñar el modelado de software mediante la estructura estática y dinámica de UML  (Casos de uso, clases, secuencia, componentes, despliegue, estado).

Ser
Sistemático.
Analítico.
Observador.
Crítico.
Colaborativo.
Ético.

########################################################

1.	Unidad de aprendizaje	III. Procesos de negocios
2.	Horas Teóricas	2
3.	Horas Prácticas	10
4.	Horas Totales	12
5.	Objetivo de la Unidad de Aprendizaje	El alumno representará los procesos de las organizaciones a través de los diagramas UML para comprender el modelo de negocio.

Temas
Modelado de negocios.
Diagrama de contexto.

Saber
Describir los conceptos de representación de los procesos de negocios.
Describir las relaciones del sistema con su entorno dentro de la organización mediante diagramas UML (distribución).

Saber Hacer
Esquematizar los procesos de negocio actuales a través de diagramas UML (roles y actividades).
Elaborar diagramas UML (distribución) que describan la interacción de los componentes de software con respecto a la organización.

Ser
Sistemático.
Analítico.
Crítico.
Coherente.
Colaborativo.
Asertivo.
Organizado.

########################################################

1.	Unidad de aprendizaje	IV. Metodologías de desarrollo de software
2.	Horas Teóricas	7
3.	Horas Prácticas	18
4.	Horas Totales	25
5.	Objetivo de la Unidad de Aprendizaje	El alumno identificará las metodologías más comunes para el desarrollo de software.

Temas
Metodologías de desarrollo tradicionales: cascada, modelo en V y espiral.
Metodologías de desarrollo ágiles: Scrum y XP.
Metodologías de desarrollo Web.

Saber
Definir los conceptos de las metodologías tradicionales de desarrollo  (cascada, modelo en V y espiral).
Distinguir las ventajas y desventajas de las metodologías tradicionales de desarrollo.
Definir los conceptos de las metodologías de desarrollo ágiles  (Scrum y XP). 
Distinguir las ventajas y desventajas de las metodologías de desarrollo ágiles.
Explicar la extensión WAE (Web Aplication Extension).
Identificar los estereotipos de UML para el desarrollo de una aplicación Web.

Saber Hacer
Seleccionar la metodología apropiada de acuerdo al tipo de proyecto.
Seleccionar la metodología ágil que se adapte a las condiciones de un proyecto de software.
Elaborar el modelado de aplicaciones Web mediante la estructura estática y dinámica de UML  (Casos de uso, clases, secuencia, componentes, despliegue, estado), aplicando la extensión WAE.

Ser
Sistemático.
Analítico.
Crítico.
Coherente.
Colaborativo.
Asertivo.
Organizado.

########################################################